#!/usr/bin/env node
const program = require("commander")
const { bootstrap } = require("./bootstrap")

program
  .option("-n, --name <String>", "Name of component", "MyComponent")
  .option("-p, --path <String>", "Path of directory to create new component")
  .action(bootstrap)
  .parse(process.argv)
