module.exports = {
  extends: ["eslint-config-with-prettier"],
  rules: {
    "jest/valid-describe": 0,
    "linebreak-style": 0,
    "prettier/prettier": [
      "error",
      {
        semi: false,
        printWidth: 100
      }
    ],
    "class-methods-use-this": 0,
    "no-underscore-dangle": 0
  }
}

