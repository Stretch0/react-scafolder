const fs = require("fs")

jest.mock("fs")

const {
	getTemplateFilePaths,
	getSingleTemplate,
	getTemplates,
	copyTemplateToNewLocation,
	bootstrap
} = require("./bootstrap")

describe("bootstrap", () => {

	beforeEach(() => {
		jest.resetAllMocks()
	})

	describe("getTemplateFilePaths", () => {

		it("Should return array of template file paths and information", () => {
			const templateFilePaths = getTemplateFilePaths("MyComponent")
			expect(templateFilePaths).toEqual([
			  {
			    "name": "index",
			    "type": "js",
			    "filepath": `${__dirname}/templates/functionalComponent/index.js`
			  },
			  {
			    "name": "MyComponent",
			    "type": "js",
			    "filepath": `${__dirname}/templates/functionalComponent/functionalComponent.js`
			  },
			  {
			    "name": "MyComponent",
			    "type": "test.js",
			    "filepath": `${__dirname}/templates/functionalComponent/functionalComponent.test.js`
			  },
			  {
			    "name": "MyComponent",
			    "type": "styles.js",
			    "filepath": `${__dirname}/templates/functionalComponent/functionalComponent.styles.js`
			  }
			])
		})
	})

	describe("getSingleTemplate", () => {

		it("Should get a single template based on proivded path", async () => {
			fs.readFileSync = jest.fn().mockReturnValue(`export { default } from "./{{component}}"`)
			const data = { 
				filepath: "./templates/functionalComponent/index.js", 
				name: "index", 
				type: "js"
			}
			const componentName = "MyComponent"

			const template = await getSingleTemplate(data, componentName)
			expect(template).toEqual({"name": "index", "template": "export { default } from \"./MyComponent\"", "type": "js"})
			expect(fs.readFileSync).toHaveBeenCalledWith("./templates/functionalComponent/index.js", "utf8")
		})
	})

	describe("getTemplates", () => {

		it("Should get all templates", async () => {
			fs.readFileSync = jest.fn().mockReturnValue(`export { default } from "./{{component}}"`)
			const componentName = "MyComponent"
			const templates = await getTemplates(componentName)
			expect(templates).toEqual([
			  {
			    "template": "export { default } from \"./MyComponent\"",
			    "name": "index",
			    "type": "js"
			  },
			  {
			    "template": "export { default } from \"./MyComponent\"",
			    "name": "MyComponent",
			    "type": "js"
			  },
			  {
			    "template": "export { default } from \"./MyComponent\"",
			    "name": "MyComponent",
			    "type": "test.js"
			  },
			  {
			    "template": "export { default } from \"./MyComponent\"",
			    "name": "MyComponent",
			    "type": "styles.js"
			  }
			])
		})
	})

	describe("copyTemplateToNewLocation", () => {

		it("Should copy template to new location", async () => {
			fs.writeFileSync = jest.fn()
			const args = {
				template: "template string", 
				name: "MyComponent", 
				type: "js", 
				folder: "."
			}
			await copyTemplateToNewLocation(args)

			expect(fs.writeFileSync).toHaveBeenCalledWith("MyComponent.js", "template string")
			
		})
	})


})