const fs = require("fs")
const path = require("path")

const getTemplateFilePaths = name => [
  {
    name: "index",
    type: "js",
    filepath: path.join(__dirname, "./templates/functionalComponent/index.js")
  },
  {
    name,
    type: "js",
    filepath: path.join(__dirname, "./templates/functionalComponent/functionalComponent.js")
  },
  {
    name,
    type: "test.js",
    filepath: path.join(__dirname, "./templates/functionalComponent/functionalComponent.test.js")
  },
  {
    name,
    type: "styles.js",
    filepath: path.join(__dirname, "./templates/functionalComponent/functionalComponent.styles.js")
  }
]

const getSingleTemplate = ({ filepath, name, type }, componentName) => ({
  template: fs.readFileSync(filepath, "utf8").replace(/{{component}}/g, componentName, ""),
  name,
  type
})

const getTemplates = async name => {
  try {
    const templateFilePaths = getTemplateFilePaths(name)
    const promises = templateFilePaths.map(paths => getSingleTemplate(paths, name))

    return Promise.all(promises)
  } catch (err) {
    throw new Error(`Unable to copy template ${err.message}`)
  }
}

const copyTemplateToNewLocation = async ({ template, name, type, folder }) => {
  try {
    const newPath = path.join(folder, `${name}.${type}`)

    return fs.writeFileSync(newPath, template)
  } catch (err) {
    throw new Error(`Unable to copy template to new location ${err.message}`)
  }
}

const bootstrap = async ({ name, path: filepath = process.env.PWD }) => {
  try {
    const templates = await getTemplates(name)

    const folder = path.join(filepath, name)

    if (!fs.existsSync(folder)) {
      await fs.mkdirSync(folder, { recursive: true })
    }

    const promises = templates.map(template => copyTemplateToNewLocation({ ...template, folder }))
    await Promise.all(promises)

    /* eslint no-console: "off" */
    console.log(`Created new component ${name} in ./${filepath}`)
  } catch (err) {
    throw new Error(`Unable to create new component ${name}: ${err.message}`)
  }
}

module.exports = {
  getTemplateFilePaths,
  getSingleTemplate,
  getTemplates,
  copyTemplateToNewLocation,
  bootstrap
}
