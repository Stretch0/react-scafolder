import { shallow } from "enzyme"
import {{component}} from "./{{component}}"

describe("{{component}}", () => {
	
	it("Should render", () => {
		const props = {}
		const component = shallow(<{{component}} {...props} />)
		expect(component).toHaveLength(1)
	})
	
})