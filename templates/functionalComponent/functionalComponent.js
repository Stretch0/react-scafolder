import React from "react"
import { Styled{{component}} } from "./{{component}}.styles"

const {{component}} = (props) => (
	<Styled{{component}}>{{component}}</Styled{{component}}>
)

export default {{component}}